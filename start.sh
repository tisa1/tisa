#!/bin/bash

instance=$1

export CERTIFICATE_DIR="certificates/localhost"
export NODE_ENV="development"

export ORIGIN="https://localhost"
export MODE="development"
export JWT_SECRET="R5r7TcDeRj9iCrBABFCyBNcyDKpfhfxgpbb95hhg"
export STRIPE_SECRET="sk_test_51ItgLlLjbeBKS6QKShj4KpGb1zzPyhZJAnA3v98xl2dCyxmEq3DzeNFFRVp1voDvjNMKvc47o3DsQc1ruxFAdtdG00jESAuwRJ"
export MONGO_URI="mongodb+srv://tisa-developers:UmNFPhBmVeiZOjtC@tisa.xfast.mongodb.net/tisa-dev"

export DIGITAL_OCEAN_SPACES_ENDPOINT=""
export DIGITAL_OCEAN_SPACES_ACCESS_KEY_ID=""
export DIGITAL_OCEAN_SPACES_SECRET_ACCESS_KEY=""
export DIGITAL_OCEAN_SPACES_BUCKET=""


# Running the project in development mode
docker-compose -f launch.yaml up --build
